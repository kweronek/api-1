package apiGlobal

import (
	"api-1/model"
	"fmt"
)

//initAPI
func InitAPI() {
	model.ResourcesInit()
	model.ThingsInit()
	model.ItemsInit()
	fmt.Println("init API!")
}
