package apiGlobal

type ServiceGlobal struct {
	APIname     string
	INetDomain  string
	Port        string
	Version     string
	ReleaseDate string
	Mail        string
}

var SvcGlob = ServiceGlobal{
	"API-1",
	"drkw.de",
	":8080",
	"0.3.0",
	"28.12.2020 12:36 MEST",
	"KWeronek@Fb2.fra-uas.de",
}

// generate, maintain newID
type RecordCounter struct {
	Count int
}

var RecCnt = RecordCounter{3}

func (RecCnt *RecordCounter) NextValue() int {
	RecCnt.Count++
	return RecCnt.Count
}

func (RecCnt *RecordCounter) SetValue(c int) int {
	RecCnt.Count = c
	return RecCnt.Count
}
