package funcs

import (
	"log"
	"net/http"
)

func LogRequest(r *http.Request) {
	log.Printf("%s %s %s %s\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])
	return
}
