package model

import (
	"encoding/json"
	"fmt"
	"os"
)

// The model
type Resource struct {
	ID       int     `json:"ID"`
	Vorname  string  `json:"Vorname"`
	Nachname string  `json:"Nachname"`
	Alter    int     `json:"Alter"`
	Score    float64 `json:"Score"`
	Present  bool    `json:"Present"`
}

func CheckResource(ID int) (exists bool) {
	_, exists = MMyResources[ID]
	return exists
}

func ValidateResource(toBeValidated string) (bool, error) {
	//	fmt.Println("validate resources")
	//	not implemented yet
	var err error = nil
	return true, err
}

// SELECT * FROM RESOURCE
// func SelectFrom(table Resource)(result ResourceTable){
//	return nil
//^}

// SELECT * FROM RESOURCE where ID=idVal
func SelectFromWhere(table Resource, idVal int) (resultRecord Resource) {
	return
}

// INSERT INTO table_name VALUES (value1, value2, value3, ...);

// INSERT RESOURCE
func Insert(newRecord Resource) (newID int) {
	return
}

// DELETE * FROM RESOURCE WHERE ID=idVal
func Delete(table Resource, idVal int) (err error) {
	return nil
}

// IF EXISTS (SELECT TOP 1 1 FROM resources WHERE ID=IDval)
func Exists(IDval int) (err error) {
	return
}

var MMyResources = make(map[int]Resource)

var TestResource = Resource{0,
	"Testuser", "Testname", 105, 57.8, true}

// Deklarationen
func ResourcesInit() {
	// some data for test:
	MMyResources[0] = TestResource
	MMyResources[1] = Resource{
		ID:       1,
		Vorname:  "Erika",
		Nachname: "Musterfrau",
		Alter:    35,
		Score:    1.75,
		Present:  true,
	}
	MMyResources[2] = Resource{2,
		"Hans", "Mustermann", 56, 3.5, true}
	MMyResources[3] = Resource{3,
		"Kim", "Muster", 45, 1.66e-25, true}

	fmt.Println("MMyResources initialised!")

	writer := os.Stdout
	enc := json.NewEncoder(writer)
	err := enc.Encode(MMyResources)
	if err != nil {
		fmt.Println(err)
	}

	/*
		fmt.Println("MyResources initialised!")
		enc.Encode(&MMyResources)

		//	part for maps
		var MResourceA = MResource{
			"ID":       "1",
			"Vorname":  "Max",
			"Nachname": "Mustermann",
			"Alter":    "34",
			"Score":    "1.45",
		}
		var MResourceB = MResource{
			"ID":       "2",
			"Vorname":  "Erika",
			"Nachname": "Musterfrau",
			"Alter":    "34",
			"Score":    "1.45",
		}

		var MResourceC = MResource{
			"ID":       "3",
			"Vorname":  "Kim",
			"Nachname": "Muster",
			"Alter":    "45",
			"Score":    "1.65",
		}

		var MResourceEmtpy = MResource{
			"ID":       "",
			"Vorname":  "",
			"Nachname": "",
			"Alter":    "",
			"Score":    "",
		}
	*/
	/*
		MMyResources = make(map[int]resources)
		MMyResources[0] = TestResource
		MMyResources[1] = MyResources[1]
		MMyResources[2] = MyResources[2]
		MMyResources[3] = MyResources[3]
	*/
	/*
	   for i := 0; i < len(MMyResources); i++ {

	   		fmt.Printf("%resources %2d %resources %resources %resources %resources %resources %resources\n",
	   			"Nr.", i, ":",
	   			MMyResources[i]["ID"],
	   			MMyResources[i]["Vorname"],
	   			MMyResources[i]["Nachname"],
	   			MMyResources[i]["Alter"],
	   			MMyResources[i]["Score"], )
	   	}
	*/
}
