package model

import (
	"encoding/json"
	"fmt"
	"os"
)

// The item Model
type Item struct {
	ID           int     `json:"ID"`
	ItemID       int     `json:"ItemID"`
	Beschreibung string  `json:"Beschreibung"`
	Preis        float64 `json:"Preis"`
	Present      bool    `json:"Present"`
}

func CheckItem(ID int) (exists bool) {
	_, exists = MMyItems[ID]
	return exists
}

var TestItem = Item{0, 0, "Testitem", 57.8, true}

var MMyItems = make(map[int]Item)

func ItemsInit() {
	MMyItems[0] = TestItem
	fmt.Println("MMyItems initialised!")

	writer := os.Stdout
	enc := json.NewEncoder(writer)
	err := enc.Encode(MMyItems)
	if err != nil {
		fmt.Println(err)
	}
}
