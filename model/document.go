package model

import (
	"encoding/json"
	"fmt"
	"os"
)

// The document Model
type Document struct {
	ID           int    `json:"ID"`
	Beschreibung string `json:"Beschreibung"`
}

func CheckDocument(ID int) (exists bool) {
	_, exists = MMyDocuments[ID]
	return exists
}

var TestDocument = Document{0, "Testdocument"}

var MMyDocuments = make(map[int]Document)

func ItemDocument() {
	MMyDocuments[0] = TestDocument
	fmt.Println("MMyDocuments initialised!")

	writer := os.Stdout
	enc := json.NewEncoder(writer)
	err := enc.Encode(MMyItems)
	if err != nil {
		fmt.Println(err)
	}
}
