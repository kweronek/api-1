package model

import (
	"encoding/json"
	"fmt"
	"os"
)

// The model
type Thing struct {
	ID      int     `json:"ID"`
	Name    string  `json:"Vorname"`
	Number  int     `json:"Alter"`
	Score   float64 `json:"Score"`
	Present bool    `json:"Present"`
}

var TestThing = Thing{0, "Testuser", 105, 57.8, true}

var MMyThings = make(map[int]Thing)

// Deklarationen
func ThingsInit() {
	// some data for test:
	MMyThings[0] = TestThing
	MMyThings[1] = Thing{
		ID:      1,
		Name:    "Erika",
		Number:  35,
		Score:   1.75,
		Present: true,
	}
	MMyThings[2] = Thing{2,
		"Hans", 56, 3.5, true}
	MMyThings[3] = Thing{3,
		"Kim", 45, 1.66e-25, true}

	fmt.Println("MMyThing initialised!")

	var writer = os.Stdout
	enc := json.NewEncoder(writer)
	err := enc.Encode(MMyThings)
	if err != nil {
		fmt.Println(err)
	}

	/*
		fmt.Println("MyResources initialised!")
		enc.Encode(&MMyResources)

		//	part for maps
		var MResourceA = MResource{
			"ID":       "1",
			"Vorname":  "Max",
			"Nachname": "Mustermann",
			"Alter":    "34",
			"Score":    "1.45",
		}
		var MResourceB = MResource{
			"ID":       "2",
			"Vorname":  "Erika",
			"Nachname": "Musterfrau",
			"Alter":    "34",
			"Score":    "1.45",
		}

		var MResourceC = MResource{
			"ID":       "3",
			"Vorname":  "Kim",
			"Nachname": "Muster",
			"Alter":    "45",
			"Score":    "1.65",
		}

		var MResourceEmtpy = MResource{
			"ID":       "",
			"Vorname":  "",
			"Nachname": "",
			"Alter":    "",
			"Score":    "",
		}
	*/
	/*
		MMyResources = make(map[int]resources)
		MMyResources[0] = TestResource
		MMyResources[1] = MyResources[1]
		MMyResources[2] = MyResources[2]
		MMyResources[3] = MyResources[3]
	*/
	/*
	   for i := 0; i < len(MMyResources); i++ {

	   		fmt.Printf("%resources %2d %resources %resources %resources %resources %resources %resources\n",
	   			"Nr.", i, ":",
	   			MMyResources[i]["ID"],
	   			MMyResources[i]["Vorname"],
	   			MMyResources[i]["Nachname"],
	   			MMyResources[i]["Alter"],
	   			MMyResources[i]["Score"], )
	   	}
	*/
}
