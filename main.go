package main

import (
	"api-1/apiGlobal"
	"api-1/dbServer"
	"api-1/webServer"
)

func main() {

	// connect database
	dbServer.ConnectDatabase()

	// next line just generates some test data records:
	apiGlobal.InitAPI()

	// start webserver
	webServer.StartWebserver()
}
