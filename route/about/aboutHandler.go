package about

import (
	"api-1/apiGlobal"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func AboutHandleFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("%resources %resources %resources %resources\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])
	// response for version information
	switch r.Header.Get("Content-Type") {

	case "application/json":
		w.Header().Set("Content-Type", "application/json")
		jsonData, err := json.Marshal(apiGlobal.SvcGlob)
		if err == nil {
			_, _ = fmt.Fprint(w, string(jsonData))
		} else {
			log.Print("** internal Server Error ", err)
			w.WriteHeader(http.StatusInternalServerError)
		}

	case "text/plain":
		w.Header().Set("Content-Type", "text/plain")
		_, _ = fmt.Fprint(w,
			apiGlobal.SvcGlob.APIname,
			apiGlobal.SvcGlob.Version,
			apiGlobal.SvcGlob.ReleaseDate,
		)

	default:
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		t, err := template.ParseFiles("resources/about/aboutTemplate.html")
		if err != nil {
			log.Print("** unable to load template aboutTemplate.html ! ", err)
		} else {
			err = t.Execute(w, apiGlobal.SvcGlob)
			if err != nil {
				log.Print(err)
			}
		}
	}
}
