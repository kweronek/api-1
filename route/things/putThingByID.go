package things

import (
	"api-1/apiGlobal"
	"api-1/model"
	"encoding/json"
	"log"
	"net/http"
)

func PutThing(ID int, putJSON string) (httpStatus int) {
	var putThing model.Thing
	if json.Unmarshal([]byte(putJSON), &putThing) != nil {
		log.Println("** Data not valid")
		return http.StatusPartialContent
	}
	model.MMyThings[ID] = putThing
	if ID > apiGlobal.RecCnt.Count {
		apiGlobal.RecCnt.SetValue(ID)
	}
	return http.StatusCreated
}
