package things

import (
	"api-1/apiGlobal/funcs"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

// ********************************************************************************************************************
// Handler for resources with ID specified
// ********************************************************************************************************************
func ThingsHandleFuncByID(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s %s %s\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])
	var pp = funcs.ParsePath(r.URL.Path)

	ID, err := strconv.Atoi(pp[1])
	if err != nil {
		if pp[1] == "" {
			log.Print(pp[1] + "** no resources specified!")
		} else {
			log.Print("** \"" + pp[1] + "\" is not a number")
			w.WriteHeader(http.StatusBadRequest)
		}
		return
	}

	switch r.Method {
	case "GET": // read a single resources
		thingBuf, httpStatus := GetThingByID(ID)
		if httpStatus != 200 {
			w.WriteHeader(http.StatusNotFound)
		} else {

			switch r.Header.Get("Accept") {

			case "": // this order leads to better performance
				fallthrough
			case "application/json":
				fallthrough
			default:
				w.Header().Set("Content-Type", "application/json")
				jsonData, err := json.Marshal(thingBuf)
				if err != nil {
					log.Print(err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				fmt.Fprint(w, string(jsonData))

			case "text/html":
				w.Header().Set("Content-Type", "text/html; charset=utf-8")
				t, err := template.ParseFiles("views/resources.html")
				if err != nil {
					log.Print(w, "Unable to load template")
				} else {
					err = t.Execute(w, thingBuf)
					if err != nil {
						log.Print(err)
					}
				}
			case "text/plain":
				w.Header().Set("Content-Type", "text/plain")
				fmt.Fprint(w, thingBuf)
			}
		}

	case "POST": // create a new things
		var pp = funcs.ParsePath(r.URL.Path)
		if pp[1] != "" {
			w.WriteHeader(http.StatusBadRequest)
			log.Print("** bad request")
			return
		}

		var requestBody, httpStatus = funcs.ReqBody(r)
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print("** httpStatus: ", httpStatus)
		} else {
			switch r.Header.Get("Content-Type") {
			case "":
				fallthrough
			case "application/json":
				newID, httpStatus := PostThing(requestBody)
				w.WriteHeader(httpStatus)
				fmt.Println(newID)
				fmt.Fprint(w, "{\nnew ID:\t", newID, "\n}")
			default:
				w.WriteHeader(http.StatusUnsupportedMediaType)
				log.Print("** unsupported media type")
			}
		}

	case "PUT": // replace a things completely
		var pp = funcs.ParsePath(r.URL.Path)
		pID, err := strconv.Atoi(pp[1])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		switch r.Header.Get("Content-Type") {
		case "", "application/json":
			requestBody, httpStatus := funcs.ReqBody(r)
			if httpStatus != 200 {
				w.WriteHeader(httpStatus)
				return
			} else {
				httpStatus = PutThing(pID, requestBody)
				w.WriteHeader(httpStatus)
			}
		default:
			w.WriteHeader(http.StatusUnsupportedMediaType)
			log.Print("** unsupported media type")
		}

	case "PATCH": // change parts of a things
		var pp = funcs.ParsePath(r.URL.Path)
		pID, err := strconv.Atoi(pp[1])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var requestBody, httpStatus = funcs.ReqBody(r)
		if httpStatus != 200 {
			w.WriteHeader(httpStatus)
			return
		} else {
			switch r.Header.Get("Content-Type") {
			case "", "application/json":
				httpStatus := PatchThing(pID, requestBody)
				w.WriteHeader(httpStatus)
			default:
				w.WriteHeader(http.StatusUnsupportedMediaType)
				log.Print("** unsupported media type")
			}
		}

	case "DELETE": //
		if CheckThing(ID) {
			DeleteThing(ID)
		} else {
			log.Println("** resources ", ID, "does not exist!")
		}

	default: // all unsupported methods
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Println("** method: " + r.Method + "not supported for this URI!\n")
	}
}

/*
 // ********************************************************************************************************************
 // handler for things without ID
 // ********************************************************************************************************************
 func ThingsHandleFunc(w http.ResponseWriter, r *http.Request) {
 	log.Printf("%resources %resources %resources %resources\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])

 	switch r.Method {

 	case "GET":
 		var result, httpStatus = things.GetAllThings()
 		if httpStatus != 200 {
 			w.WriteHeader(http.StatusBadRequest)
 			log.Print(httpStatus)
 		} else {
 			w.Header().Set("Content-Type", "application/json")
 			fmt.Fprintf(w, result)
 		}
 	case "POST":
 		requestBody, httpStatus := funcs.ReqBody(r)
 		if httpStatus != 200 {
 			w.WriteHeader(http.StatusBadRequest)
 			log.Print(httpStatus)
 		} else {
 			result, httpStatus := things.PostThing(requestBody)
 			w.WriteHeader(httpStatus)
 			fmt.Fprint(w, result)
 		}

 	default:
 		w.WriteHeader(http.StatusMethodNotAllowed)
 		log.Print("** method ", r.Method, " not supported without ID")
 	}
 }


*/
