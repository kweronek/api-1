package things

import (
	"api-1/model"
	"encoding/json"
	"net/http"
)

func GetAllThings() (result string, httpStatus int) {
	var jsonData []byte
	//	dbServer.SelectFrom(modelResource.resources())
	var err error
	jsonData, err = json.Marshal(model.MMyThings)
	if err != nil {
		return "", http.StatusUnauthorized
	} else {
		return string(jsonData), http.StatusOK
	}
}
