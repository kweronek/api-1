package things

import "api-1/model"

// SELECT * FROM RESOURCE
// func SelectFrom(table Resource)(result ResourceTable){
//	return nil
//^}

// SELECT * FROM RESOURCE where ID=idVal
func SelectFromWhere(table model.Thing, idVal int) (resultRecord model.Thing) {
	return
}

// INSERT INTO table_name VALUES (value1, value2, value3, ...);

// INSERT RESOURCE
func Insert(newRecord model.Thing) (newID int) {
	return
}

// DELETE * FROM RESOURCE WHERE ID=idVal
func Delete(table model.Thing, idVal int) (err error) {
	return nil
}

// IF EXISTS (SELECT TOP 1 1 FROM resources WHERE ID=IDval)
func Exists(IDval int) (err error) {
	return
}
