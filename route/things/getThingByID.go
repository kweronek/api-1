package things

import (
	"api-1/model"
	"log"
	"net/http"
)

func GetThingByID(ID int) (result model.Thing, httpStatus int) {
	if CheckThing(ID) {
		return model.MMyThings[ID], http.StatusOK
	} else {
		log.Println("** resources", ID, "does not exist!")
		return model.Thing{}, http.StatusNotFound
	}
}
