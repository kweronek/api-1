package document

import (
	"api-1/apiGlobal/funcs"
	"fmt"
	"log"
	"net/http"
)

// ********************************************************************************************************************
// handler for documents without ID
// ********************************************************************************************************************

/*
type GET string
func (g GET) Method() {
	fmt.Println("GET")
}
type PUT string
func (put PUT) Method() {
	fmt.Println("PUT")
}
/*
func (g Get) w http.ResponseWriter {
	var result, httpStatus = GetAllDocuments()
	if httpStatus != 200 {
		w.WriteHeader(http.StatusBadRequest)
		log.Print(httpStatus)
	} else {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, result)
	}
}
*
type Put http.ResponseWriter
func PutFunc (w http.ResponseWriter, r *http.Request) {

	requestBody, httpStatus := funcs.ReqBody(r)
	if httpStatus != 200 {
		w.WriteHeader(http.StatusBadRequest)
		log.Print(httpStatus)
	} else {
		result, httpStatus := PostAllDocuments(requestBody)
		w.WriteHeader(httpStatus)
		_, _ = fmt.Fprint(w, result)
	}
}
*/
/*
type HttpRequestI interface {
	Method()
}
func DocumentsHandleFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s %s %s\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])
var myRequest HttpRequestI
var myMethod = "GET"
var mym = interface{}(myMethod)
myRequest.mym()


var myHttpRequestI HttpRequestI
	myHttpRequestI = *r
	myHttpRequest.r.Method
}
*/

func DocumentsHandleFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s %s %s\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])

	switch r.Method {

	case "GET":

		var result, httpStatus = GetAllDocuments()
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print(httpStatus)
		} else {
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprintf(w, result)
		}
	case "POST":
		requestBody, httpStatus := funcs.ReqBody(r)
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print(httpStatus)
		} else {
			result, httpStatus := PostAllDocuments(requestBody)
			w.WriteHeader(httpStatus)
			_, _ = fmt.Fprint(w, result)
		}

	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Print("** method ", r.Method, " not supported without ID")
	}
}
