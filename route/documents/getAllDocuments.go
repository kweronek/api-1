package document

import (
	"api-1/model"
	"encoding/json"
	"log"
	"net/http"
)

func GetAllDocuments() (result string, httpStatus int) {
	log.Println("GetAllDocuments")
	var jsonData []byte
	//	dbServer.SelectFrom(modelResource.resources())
	var err error
	jsonData, err = json.Marshal(model.MMyDocuments)
	if err != nil {
		return "", http.StatusInternalServerError
	} else {
		return string(jsonData), http.StatusOK
	}
}
