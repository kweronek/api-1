package document

import (
	"api-1/apiGlobal"
	"api-1/model"
	"encoding/json"
	"log"
	"net/http"
)

func PostAllDocuments(body string) (Result string, HttpStatus int) {
	log.Println("PostAllDocuments")

	var toBeCreated = make([]model.Document, 1, 1)
	var indexCreated = make([]int, 0, 0)

	// JSON array:
	if body[0] == '[' {
		err := json.Unmarshal([]byte(body), &toBeCreated)
		if err != nil {
			log.Println("** could not create new resources")
			return "{\"ID\": -1}", http.StatusPartialContent
		} else {
			for i := 0; i < len(toBeCreated); i++ {
				NewID := apiGlobal.RecCnt.NextValue()
				model.MMyDocuments[NewID] = toBeCreated[i]
				indexCreated = append(indexCreated, NewID)
			}
			jsonData, err := json.Marshal(indexCreated)
			if err != nil {
				log.Println("** could not create new resources")
				return "{\"ID\": -1}", http.StatusInternalServerError
			}
			return "{\"ID\": " + string(jsonData) + "}", http.StatusCreated
		}
	}
	return "{\"ID\": -1}", http.StatusBadRequest
}

/*
	// JSON single object
	if body[0] == '{' {
		err := json.Unmarshal([]byte(body), &toBeCreated[0])
		if err != nil {
			log.Println("** could not create new resources")
			return "{\"ID\": -1}", http.StatusPartialContent
		} else {
			NewID := apiGlobal.RecCnt.NextValue()
			models.MMyResources[NewID] = toBeCreated[0]
			return "{\"ID\": " + strconv.Itoa(NewID) + "}", http.StatusCreated
		}
	}
*/
