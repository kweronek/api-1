package document

import (
	"api-1/model"
	"log"
	"net/http"
)

func GetDocument(ID int) (result model.Document, httpStatus int) {
	if model.CheckDocument(ID) {
		return model.MMyDocuments[ID], http.StatusOK
	} else {
		log.Println("** ID", ID, "does not exist!")
		return model.Document{}, http.StatusNotFound
	}
}

/*
func GetAllResources() (result string, httpStatus int) {
	var jsonData []byte
	//	dbServer.SelectFrom(modelResource.resources())
	var err error
	jsonData, err = json.Marshal(models.MMyResources)
	if err != nil {
		return "", http.StatusUnauthorized
	} else {
		return string(jsonData), http.StatusOK
	}
}
*/
