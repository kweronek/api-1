package route

import (
	"log"
	"net/http"
)

func FaviconHandleFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("%resources %resources %resources %resources\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])
	// response for version information
	w.Header().Set("Content-Type", "image/x-icon")
	w.Header().Set("Cache-Control", "public, max-age=7776000")
	http.ServeFile(w, r, "apiGlobal/favicon.ico")
}
