package home

import (
	"api-1/apiGlobal"
	"api-1/apiGlobal/funcs"
	//	"api-1/resources/thingsByID"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

// home page
func HomeHandleFunc(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s %s %s\n", r.Method, r.Host+r.URL.Path, r.Proto, r.Header["Content-Type"])

	var pp = funcs.ParsePath(r.URL.Path)
	if (pp[0] != "home") && (pp[0] != "") {
		log.Println("** " + r.URL.Path + " is not a valid resources!")
	}

	if r.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Println("** HTTP-method " + r.Method + " not supported here. Use GET!")
	} else {
		switch r.Header.Get("Accept") {

		default:
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			var t, err = template.ParseFiles("route/home/homeTemplate.html")
			if err != nil {
				log.Print("** Unable to load template! ", err)
				return
			}
			err = t.Execute(w, apiGlobal.SvcGlob)
			if err != nil {
				log.Println("** t.Execute not possible! ", err)
			}

		case "text/plain":
			w.Header().Set("Content-Type", "text/plain")
			_, _ = fmt.Fprint(w,
				"KWer'resources CRUD-REST-API for resources: ",
				apiGlobal.SvcGlob.Version,
				" 08/06/2019 11:56 MEST.")
		}
	}
}
