package items

import (
	"api-1/model"
	"log"
	"net/http"
)

func GetItem(ID int) (result model.Item, httpStatus int) {
	if model.CheckItem(ID) {
		return model.MMyItems[ID], http.StatusOK
	} else {
		log.Println("** ID", ID, "does not exist!")
		return model.Item{}, http.StatusNotFound
	}
}
