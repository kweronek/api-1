package route

import (
	"api-1/route/about"
	"api-1/route/documents"
	"api-1/route/home"
	"api-1/route/resources"
	"api-1/route/things"
	"net/http"
)

func SetRoutes() {
	http.HandleFunc("/about", about.AboutHandleFunc)
	http.HandleFunc("/About", about.AboutHandleFunc) // optional

	http.HandleFunc("/documents", document.DocumentsHandleFunc)
	//	http.HandleFunc("/documents/", document.DocumentsHandleFuncByID)
	//	http.HandleFunc("/document/", document.ResourcesHandleFuncByID) // optional

	http.HandleFunc("/favicon.ico", FaviconHandleFunc)

	http.HandleFunc("/", home.HomeHandleFunc)

	http.HandleFunc("/resources", resource.ResourcesHandleFunc)
	http.HandleFunc("/resources/", resource.ResourcesHandleFuncByID)
	http.HandleFunc("/resource/", resource.ResourcesHandleFuncByID) // optional

	http.HandleFunc("/things", things.ThingsHandleFunc)
	http.HandleFunc("/things/", things.ThingsHandleFuncByID)
	http.HandleFunc("/thing/", things.ThingsHandleFuncByID) // optional

	//  bei Verwendung von muxern
	//	resources.router.HandleFunc("/api/", resources.handleAPI())
	//	resources.router.HandleFunc("/about", resources.handleAbout())
	//	resources.router.HandleFunc("/", resources.handleIndex())

}
