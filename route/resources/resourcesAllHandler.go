package resource

import (
	"api-1/apiGlobal/funcs"
	//	"encoding/json"
	"fmt"
	//	utils "helper"

	//	"helper"
	//	"html/template"
	"log"
	"net/http"
	//	"strconv"
)

// ********************************************************************************************************************
// handler for resources without ID
// ********************************************************************************************************************
func ResourcesHandleFunc(w http.ResponseWriter, r *http.Request) {
	funcs.LogRequest(r)

	switch r.Method {

	case "GET":
		var result, httpStatus = GetAllResources()
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print(httpStatus)
		} else {
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprintf(w, result)
		}
	case "POST":
		requestBody, httpStatus := funcs.ReqBody(r)
		if httpStatus != 200 {
			w.WriteHeader(http.StatusBadRequest)
			log.Print(httpStatus)
		} else {
			result, httpStatus := PostAllResources(requestBody)
			w.WriteHeader(httpStatus)
			_, _ = fmt.Fprint(w, result)
		}

	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Print("** method ", r.Method, " Method not allowed here")
	}
}
