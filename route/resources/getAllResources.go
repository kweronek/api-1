package resource

import (
	"api-1/model"
	"encoding/json"
	"net/http"
)

func GetAllResources() (result string, httpStatus int) {
	var jsonData []byte
	//	dbServer.SelectFrom(modelResource.resources())
	var err error
	jsonData, err = json.Marshal(model.MMyResources)
	if err != nil {
		return "", http.StatusInternalServerError
	} else {
		return string(jsonData), http.StatusOK
	}
}
