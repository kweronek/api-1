package resource

import (
	"api-1/model"
	"log"
	"net/http"
)

func GetResource(ID int) (result model.Resource, httpStatus int) {
	if model.CheckResource(ID) {
		return model.MMyResources[ID], http.StatusOK
	} else {
		log.Println("** ID", ID, "does not exist!")
		return model.Resource{}, http.StatusNotFound
	}
}

/*
func GetAllResources() (result string, httpStatus int) {
	var jsonData []byte
	//	dbServer.SelectFrom(modelResource.resources())
	var err error
	jsonData, err = json.Marshal(models.MMyResources)
	if err != nil {
		return "", http.StatusUnauthorized
	} else {
		return string(jsonData), http.StatusOK
	}
}
*/
