package resource

import (
	"api-1/apiGlobal"
	"api-1/model"
	"encoding/json"
	"log"
	"net/http"
)

func PutResource(ID int, putJSON string) (httpStatus int) {
	var putResource model.Resource
	if json.Unmarshal([]byte(putJSON), &putResource) != nil {
		log.Println("** Data not valid")
		return http.StatusPartialContent
	}
	model.MMyResources[ID] = putResource
	if ID > apiGlobal.RecCnt.Count {
		apiGlobal.RecCnt.SetValue(ID)
	}
	return http.StatusCreated
}
