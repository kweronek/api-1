package dbServer

type Resource struct {
	ID       int     `json:"ID"`
	Vorname  string  `json:"Vorname"`
	Nachname string  `json:"Nachname"`
	Alter    int     `json:"Alter"`
	Score    float64 `json:"Score"`
	Present  bool    `json:"Present"`
}

/*
CREATE TABLE resources (	m

	ID       int     `json:"ID"`
	Vorname  string  `json:"Vorname"`
	Nachname string  `json:"Nachname"`
	Alter    int     `json:"Alter"`
	Score    float64 `json:"Score"`
	Present  bool    `json:"Present"`
);
*/
