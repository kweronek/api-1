package dbServer

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

func ConnectDatabase() {
	var Db, err = sql.Open(
		"mysql",
		"root:Haf!35SymWAM?@tcp(localhost:3306)/testDb",
	)
	if err == nil {
		fmt.Println("Database connected!")
	} else {
		fmt.Println("Database not connected!")
	}
	defer Db.Close()

	var version string

	err2 := Db.QueryRow("SELECT VERSION()").Scan(&version)
	if err2 != nil {
		log.Fatal(err2)
	}
	fmt.Println("MySQL-Version:", version)
}
