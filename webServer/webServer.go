package webServer

import (
	"api-1/apiGlobal"
	"api-1/route"
	"fmt"
	"log"
	"net/http"
	"time"
)

func StartWebserver() {

	route.SetRoutes()

	// configuration parameters for webserver
	s := &http.Server{
		Addr:           apiGlobal.SvcGlob.Port,
		Handler:        nil,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	fmt.Println("\nWebserver is started and listening on port", apiGlobal.SvcGlob.Port, "\n")
	log.Fatal(s.ListenAndServe())
}
